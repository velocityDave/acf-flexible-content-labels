<?php
/**
 * Plugin Name: ACF Flexible Content Labels
 * Plugin URI: #
 * Description: Flexible content blocks inherit the label of the first field within them.
 * Version: 1.0.0
 * Author: Dave Welch
 * Author URI: https://velocitypartners.com
 */

/* Load the javascript on the admin pages using ACF */


add_action('init',	'acf_flex_content_plugin', 5);

function acf_flex_content_plugin(){

	if( class_exists('acf') ) {

		add_action( 'acf/input/admin_enqueue_scripts', 'acf_flexible_labels', 11 );



		function acf_flexible_labels() {
			$acf_version = acf()->settings['version'];


			if( version_compare( $acf_version, '4.0', '>=' ) && version_compare( $acf_version, '5.0', '<' ) ) {
				// version 4.X
				wp_enqueue_script( 'acf_flexible_labels_js', esc_url( plugins_url( 'js/acf_flexible_labels_js.js', __FILE__ ) ), array( 'jquery' ) );
			} elseif( version_compare( $acf_version, '5.0', '>=' ) ) {
				// version 5.X
				wp_enqueue_script( 'acf_flexible_labels_js', esc_url( plugins_url( 'js/acf_flexible_labels.js', __FILE__ ) ), array( 'jquery' ) );
			}
			wp_enqueue_style( 'acf_flexible_labels_css', esc_url( plugins_url( 'css/acf_flexible_labels_css.css', __FILE__ ) ) );
		}

	}
}

?>
jQuery(document).ready(function($) {
	console.log('acf flex content labels running');



	if (jQuery('.acf-field-flexible-content').length > 0){

		function update_watch(){
			jQuery('.acf-field-flexible-content .values .layout').change(update_acf_labels);
		}

		function update_acf_labels(){
			jQuery('.acf-field-flexible-content .values .layout').each(function(){
				var $scope = jQuery(this);
				// fire collapse function (data-toggle="closed");

				var curlabel = false;
				// look for a specific admin label first
				var $watch_el = jQuery('> .acf-fields > .acf-field[data-name="admin_label"] input[type="text"]:first', $scope );
				if ($watch_el.length > 0) {
					curlabel = $watch_el.val();
					curlabel = curlabel.replace(/[\t\n]+/g,' ');
					//console.log('found admin label ' + curlabel);
				} else {
					//console.log('couldnt find admin label');
					$watch_el = jQuery('.acf-fields input[type="text"]:first', $scope );
					//console.log($watch_el.length);

					if ($watch_el.length > 0) {
						curlabel = jQuery( $watch_el.val() ).text();
						curlabel = curlabel.replace(/[\t\n]+/g,' ');
					} else {
						// traverse harder
						var $deeperlabel = jQuery('.acf-fields .wp-editor-area', $scope );
						if ($deeperlabel.length > 0) {
							//console.log($watch_el);
							//console.log('festchign first text');
							var val = '';
							$deeperlabel.each(function(){
								// fetch html contents, store as a jquery el then fetch the inner text to strip tags
								val = jQuery( $(this).val() ).text();
								// strip newlines and tabs
								val = val.replace(/[\t\n]+/g,' ');
								//console.log(val);
								//console.log(val.length);
								if (val.length > 2) {
									$watch_el = $(this);
									return;
								}
							});
							if (val !== ''){
								curlabel = val;
								//console.log('BAILING');
								//console.log(val);
							} else {
								//console.log('couldnt find a long enough string');
							}
							//$watch_el = $watch_el.eq(1).val();
							//console.log($watch_el.length);
							//$watch_el = jQuery($watch_el).text();
							//console.log($watch_el);
							//curlabel = $watch_el.eq(1).text();
						}
					}
				}

				if (curlabel){
					//console.log(curlabel);

					var $handle = jQuery('.acf-fc-layout-handle:first', $scope );

					// cap labels from being too long
					if (curlabel.length > 30) curlabel = curlabel.substr(0, 30) + '...';
					var title = $handle.html();
					//console.log(title);
					$watch_el.focusout(update_acf_labels);
					title = title.split('</span> ');
					var newLabel = title[0] + '</span> <span class="acf-original-module-title">' + title[1] + '</span> - <span class="acf-admin-label-name">' + curlabel + '</span>';
					$handle.html(newLabel);
				}

			});
		}

		function hide_all_fields(){
			jQuery( '.acf-field-flexible-content .layout' ).each(function(){
				// vars
				var $layout	= jQuery(this);
				$layout.attr('data-toggle', 'closed').addClass('-collapsed');
//				$layout.children('.acf-input-table').hide();
			});
			acf.fields.flexible_content.sync();
		}

		hide_all_fields();
		setTimeout(function(){
			update_acf_labels();
			update_watch();
			jQuery('.acf-field-flexible-content .values').change(update_watch);
		}, 500);

	}
});



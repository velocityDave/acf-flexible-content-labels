# ACF Flexible Content / Repeater Labels #

Simple plugin designed to improve the usability of the Advanced Custom Field flexible content area (or repeatable content area). The plugin will do two things:

1. Toggle all FCA/RCA's to closed on pageload
1. Update the collapsable group header to the input of the first field within the group (so this needs to be a title field)